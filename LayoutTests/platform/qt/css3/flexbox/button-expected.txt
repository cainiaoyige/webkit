layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x284
  RenderBlock {HTML} at (0,0) size 800x284
    RenderBody {BODY} at (8,8) size 784x268
      RenderBlock (anonymous) at (0,0) size 784x38
        RenderText {#text} at (0,0) size 755x38
          text run at (0,0) width 379: "Test for empty buttons, which inherit from RenderFlexibleBox. "
          text run at (379,0) width 376: "Empty <input> buttons should not collapse, which makes them"
          text run at (0,19) width 431: "different from most flex boxes. Empty <button>s should collapse. Note "
        RenderInline {A} at (0,0) size 74x19 [color=#0000EE]
          RenderText {#text} at (431,19) size 74x19
            text run at (431,19) width 74: "bug 110654"
        RenderText {#text} at (505,19) size 4x19
          text run at (505,19) width 4: "."
      RenderBlock {HR} at (0,46) size 784x2 [border: (1px inset #000000)]
      RenderBlock (anonymous) at (0,56) size 784x87
        RenderText {#text} at (0,0) size 76x19
          text run at (0,0) width 76: "Simple case."
        RenderBR {BR} at (76,0) size 0x19
        RenderButton {BUTTON} at (2,21) size 12x29 [bgcolor=#C0C0C0]
        RenderBR {BR} at (16,29) size 0x19
        RenderButton {INPUT} at (2,54) size 12x31 [bgcolor=#C0C0C0]
        RenderBR {BR} at (16,64) size 0x19
      RenderBlock {HR} at (0,151) size 784x2 [border: (1px inset #000000)]
      RenderBlock (anonymous) at (0,161) size 784x107
        RenderText {#text} at (0,0) size 739x19
          text run at (0,0) width 739: "Empty <button> and <input type=button> with overflow: scroll;. The presence of the scrollbar should not shrink the button."
        RenderBR {BR} at (739,0) size 0x19
        RenderBR {BR} at (31,19) size 0x19
        RenderBR {BR} at (31,69) size 0x19
layer at (10,195) size 27x29 clip at (10,195) size 12x14
  RenderButton {BUTTON} at (2,26) size 27x29 [bgcolor=#C0C0C0]
layer at (10,228) size 27x46 clip at (10,228) size 12x31
  RenderButton {INPUT} at (2,59) size 27x46 [bgcolor=#C0C0C0]
