layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x327
  RenderBlock {HTML} at (0,0) size 800x327
    RenderBody {BODY} at (8,16) size 784x299
      RenderBlock {P} at (0,0) size 784x19
        RenderText {#text} at (0,0) size 253x19
          text run at (0,0) width 253: "The two blocks below should be identical:"
      RenderBlock {DIV} at (12,35) size 126x126 [color=#00FFFF] [bgcolor=#008080] [border: (3px solid #000080)]
        RenderText {#text} at (27,26) size 72x73
          text run at (27,26) width 72: "X X"
          text run at (27,74) width 72: "X X"
      RenderBlock {PRE} at (12,173) size 126x126 [color=#00FFFF] [bgcolor=#008080] [border: (3px solid #000080)]
        RenderText {#text} at (3,2) size 120x121
          text run at (3,2) width 120: "     "
          text run at (123,2) width 0: " "
          text run at (3,26) width 120: " X X "
          text run at (123,26) width 0: " "
          text run at (3,50) width 120: "     "
          text run at (123,50) width 0: " "
          text run at (3,74) width 120: " X X "
          text run at (123,74) width 0: " "
          text run at (3,98) width 120: "     "
