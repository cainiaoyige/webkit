layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 740x38
          text run at (0,0) width 740: "Results that match WinIE are two columns on the right that say \"after\" every time, except for the last row which should have"
          text run at (0,19) width 145: "nothing in either column."
      RenderBlock {P} at (0,54) size 784x19
        RenderText {#text} at (0,0) size 677x19
          text run at (0,0) width 677: "Results that match Gecko are like WinIE, but with \"before\" for the attribute in the first two rows and the last row."
      RenderBlock {HR} at (0,89) size 784x2 [border: (1px inset #000000)]
      RenderBlock {FORM} at (0,99) size 784x425
        RenderTable {TABLE} at (0,0) size 772x425
          RenderTableSection {THEAD} at (0,0) size 772x25
            RenderTableRow {TR} at (0,2) size 772x21
              RenderTableCell {TH} at (2,2) size 366x21 [r=0 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 59x19
                  text run at (1,1) width 59: "test case"
              RenderTableCell {TH} at (370,2) size 278x21 [r=0 c=1 rs=1 cs=1]
                RenderText {#text} at (1,1) size 88x19
                  text run at (1,1) width 88: "form element"
              RenderTableCell {TH} at (650,2) size 59x21 [r=0 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 57x19
                  text run at (1,1) width 57: "property"
              RenderTableCell {TH} at (711,2) size 59x21 [r=0 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 57x19
                  text run at (1,1) width 57: "attribute"
          RenderTableSection {TBODY} at (0,25) size 772x400
            RenderTableRow {TR} at (0,2) size 772x29
              RenderTableCell {TD} at (2,6) size 366x21 [r=0 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 196x19
                  text run at (1,1) width 196: "text with value property changed"
              RenderTableCell {TD} at (370,2) size 278x29 [r=0 c=1 rs=1 cs=1]
                RenderTextControl {INPUT} at (3,3) size 192x23
              RenderTableCell {TD} at (650,6) size 59x21 [r=0 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,6) size 59x21 [r=0 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 39x19
                  text run at (1,1) width 39: "before"
            RenderTableRow {TR} at (0,33) size 772x29
              RenderTableCell {TD} at (2,37) size 366x21 [r=1 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 233x19
                  text run at (1,1) width 233: "password with value property changed"
              RenderTableCell {TD} at (370,33) size 278x29 [r=1 c=1 rs=1 cs=1]
                RenderTextControl {INPUT} at (3,3) size 192x23
              RenderTableCell {TD} at (650,37) size 59x21 [r=1 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,37) size 59x21 [r=1 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 39x19
                  text run at (1,1) width 39: "before"
            RenderTableRow {TR} at (0,64) size 772x21
              RenderTableCell {TD} at (2,64) size 366x21 [r=2 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 237x19
                  text run at (1,1) width 237: "check box with value property changed"
              RenderTableCell {TD} at (370,64) size 278x21 [r=2 c=1 rs=1 cs=1]
                RenderBlock {INPUT} at (4,4) size 13x13
              RenderTableCell {TD} at (650,64) size 59x21 [r=2 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,64) size 59x21 [r=2 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,87) size 772x21
              RenderTableCell {TD} at (2,87) size 366x21 [r=3 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 214x19
                  text run at (1,1) width 214: "hidden with value property changed"
              RenderTableCell {TD} at (370,96) size 278x2 [r=3 c=1 rs=1 cs=1]
              RenderTableCell {TD} at (650,87) size 59x21 [r=3 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,87) size 59x21 [r=3 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,110) size 772x37
              RenderTableCell {TD} at (2,118) size 366x21 [r=4 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 212x19
                  text run at (1,1) width 212: "button with value property changed"
              RenderTableCell {TD} at (370,110) size 278x37 [r=4 c=1 rs=1 cs=1]
                RenderButton {INPUT} at (3,3) size 39x31 [bgcolor=#C0C0C0]
                  RenderBlock (anonymous) at (6,6) size 27x19
                    RenderText at (0,0) size 27x19
                      text run at (0,0) width 27: "after"
              RenderTableCell {TD} at (650,118) size 59x21 [r=4 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,118) size 59x21 [r=4 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,149) size 772x25
              RenderTableCell {TD} at (2,151) size 366x21 [r=5 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 209x19
                  text run at (1,1) width 209: "image with value property changed"
              RenderTableCell {TD} at (370,149) size 278x25 [r=5 c=1 rs=1 cs=1]
                RenderImage {INPUT} at (1,1) size 43x23
              RenderTableCell {TD} at (650,151) size 59x21 [r=5 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,151) size 59x21 [r=5 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,176) size 772x21
              RenderTableCell {TD} at (2,176) size 366x21 [r=6 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 205x19
                  text run at (1,1) width 205: "radio with value property changed"
              RenderTableCell {TD} at (370,176) size 278x20 [r=6 c=1 rs=1 cs=1]
                RenderBlock {INPUT} at (4,4) size 12x12
              RenderTableCell {TD} at (650,176) size 59x21 [r=6 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,176) size 59x21 [r=6 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,199) size 772x29
              RenderTableCell {TD} at (2,203) size 366x21 [r=7 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 193x19
                  text run at (1,1) width 193: "text with value attribute changed"
              RenderTableCell {TD} at (370,199) size 278x29 [r=7 c=1 rs=1 cs=1]
                RenderTextControl {INPUT} at (3,3) size 192x23
              RenderTableCell {TD} at (650,203) size 59x21 [r=7 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,203) size 59x21 [r=7 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,230) size 772x21
              RenderTableCell {TD} at (2,230) size 366x21 [r=8 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 234x19
                  text run at (1,1) width 234: "check box with value attribute changed"
              RenderTableCell {TD} at (370,230) size 278x21 [r=8 c=1 rs=1 cs=1]
                RenderBlock {INPUT} at (4,4) size 13x13
              RenderTableCell {TD} at (650,230) size 59x21 [r=8 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,230) size 59x21 [r=8 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,253) size 772x21
              RenderTableCell {TD} at (2,253) size 366x21 [r=9 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 364x19
                  text run at (1,1) width 364: "text with value property changed, then turned into check box"
              RenderTableCell {TD} at (370,253) size 278x21 [r=9 c=1 rs=1 cs=1]
                RenderBlock {INPUT} at (4,4) size 13x13
              RenderTableCell {TD} at (650,253) size 59x21 [r=9 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,253) size 59x21 [r=9 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,276) size 772x29
              RenderTableCell {TD} at (2,280) size 366x21 [r=10 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 364x19
                  text run at (1,1) width 364: "check box with value property changed, then turned into text"
              RenderTableCell {TD} at (370,276) size 278x29 [r=10 c=1 rs=1 cs=1]
                RenderTextControl {INPUT} at (3,3) size 192x23
              RenderTableCell {TD} at (650,280) size 59x21 [r=10 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,280) size 59x21 [r=10 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,307) size 772x21
              RenderTableCell {TD} at (2,307) size 366x21 [r=11 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 361x19
                  text run at (1,1) width 361: "text with value attribute changed, then turned into check box"
              RenderTableCell {TD} at (370,307) size 278x21 [r=11 c=1 rs=1 cs=1]
                RenderBlock {INPUT} at (4,4) size 13x13
              RenderTableCell {TD} at (650,307) size 59x21 [r=11 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,307) size 59x21 [r=11 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,330) size 772x29
              RenderTableCell {TD} at (2,334) size 366x21 [r=12 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 361x19
                  text run at (1,1) width 361: "check box with value attribute changed, then turned into text"
              RenderTableCell {TD} at (370,330) size 278x29 [r=12 c=1 rs=1 cs=1]
                RenderTextControl {INPUT} at (3,3) size 192x23
              RenderTableCell {TD} at (650,334) size 59x21 [r=12 c=2 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
              RenderTableCell {TD} at (711,334) size 59x21 [r=12 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 27x19
                  text run at (1,1) width 27: "after"
            RenderTableRow {TR} at (0,361) size 772x37
              RenderTableCell {TD} at (2,369) size 366x21 [r=13 c=0 rs=1 cs=1]
                RenderText {#text} at (1,1) size 191x19
                  text run at (1,1) width 191: "file with value property changed"
              RenderTableCell {TD} at (370,361) size 278x37 [r=13 c=1 rs=1 cs=1]
                RenderFileUploadControl {INPUT} at (3,3) size 272x31 "No file selected"
                  RenderButton {INPUT} at (0,0) size 85x31 [bgcolor=#C0C0C0]
                    RenderBlock (anonymous) at (6,6) size 73x19
                      RenderText at (0,0) size 73x19
                        text run at (0,0) width 73: "Choose File"
              RenderTableCell {TD} at (650,378) size 59x2 [r=13 c=2 rs=1 cs=1]
              RenderTableCell {TD} at (711,369) size 59x21 [r=13 c=3 rs=1 cs=1]
                RenderText {#text} at (1,1) size 39x19
                  text run at (1,1) width 39: "before"
layer at (383,139) size 188x19
  RenderBlock {DIV} at (2,2) size 188x19
    RenderText {#text} at (0,0) size 39x19
      text run at (0,0) width 39: "before"
layer at (383,170) size 188x19
  RenderBlock {DIV} at (2,2) size 188x19
    RenderText {#text} at (0,0) size 36x19
      text run at (0,0) width 36: "\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}\x{2022}"
layer at (383,336) size 188x19
  RenderBlock {DIV} at (2,2) size 188x19
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "after"
layer at (383,413) size 188x19
  RenderBlock {DIV} at (2,2) size 188x19
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "after"
layer at (383,467) size 188x19
  RenderBlock {DIV} at (2,2) size 188x19
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "after"
