layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x38
        RenderText {#text} at (0,0) size 784x38
          text run at (0,0) width 398: "Test that search field text is vertically centered if the search field is "
          text run at (398,0) width 386: "taller than the text. In each of the following pairs of a search field"
          text run at (0,19) width 26: "and "
          text run at (26,19) width 338: "a text field, the vertical position of the text should be the "
          text run at (364,19) width 115: "same in both fields."
      RenderBlock {P} at (0,54) size 784x45
        RenderTextControl {INPUT} at (2,0) size 205x45 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
          RenderFlexibleBox {DIV} at (3,3) size 199x39
            RenderBlock {DIV} at (0,19) size 0x1
            RenderBlock {DIV} at (0,10) size 188x19
            RenderBlock {DIV} at (188,14) size 11x11
        RenderText {#text} at (209,13) size 4x19
          text run at (209,13) width 4: " "
        RenderTextControl {INPUT} at (215,0) size 194x45 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,115) size 784x20
        RenderTextControl {INPUT} at (2,0) size 205x16 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
          RenderFlexibleBox {DIV} at (3,0) size 199x16
            RenderBlock {DIV} at (0,8) size 0x0
            RenderBlock {DIV} at (0,3) size 188x10
            RenderBlock {DIV} at (188,2) size 11x12
        RenderText {#text} at (209,1) size 4x19
          text run at (209,1) width 4: " "
        RenderTextControl {INPUT} at (215,0) size 194x16 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,151) size 784x19
        RenderTextControl {INPUT} at (2,3) size 205x12 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
          RenderFlexibleBox {DIV} at (3,0) size 199x12
            RenderBlock {DIV} at (0,6) size 0x0
            RenderBlock {DIV} at (0,3) size 188x6
            RenderBlock {DIV} at (188,0) size 11x12
        RenderText {#text} at (209,0) size 4x19
          text run at (209,0) width 4: " "
        RenderTextControl {INPUT} at (215,3) size 194x12 [bgcolor=#FFFFFF] [border: (2px inset #000000)]
        RenderText {#text} at (0,0) size 0x0
layer at (13,75) size 188x19
  RenderBlock {DIV} at (0,0) size 188x19
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
layer at (226,75) size 188x19
  RenderBlock {DIV} at (3,13) size 188x19
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
layer at (13,126) size 188x10 scrollHeight 19
  RenderBlock {DIV} at (0,0) size 188x10
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
layer at (226,126) size 188x10 scrollHeight 19
  RenderBlock {DIV} at (3,3) size 188x10
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
layer at (13,165) size 188x6 scrollHeight 19
  RenderBlock {DIV} at (0,0) size 188x6
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
layer at (226,165) size 188x6 scrollHeight 19
  RenderBlock {DIV} at (3,3) size 188x6
    RenderText {#text} at (0,0) size 27x19
      text run at (0,0) width 27: "Text"
