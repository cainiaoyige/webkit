layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,540) size 800x60
  RenderBlock {HTML} at (0,0) size 800x60
    RenderBody {BODY} at (8,8) size 784x0
      RenderRegion {DIV} at (0,0) size 52x52 [border: (1px solid #000000)]
      RenderRegion {DIV} at (104,0) size 52x52 [border: (1px solid #000000)]
layer at (60,540) size 52x52
  RenderRegion {DIV} at (52,0) size 52x52 [border: (1px solid #000000)]
Flow Threads
  Thread with flow-name 'article'
    layer at (0,450) size 50x150
      RenderNamedFlowThread at (0,0) size 50x150
        RenderBlock {DIV} at (0,0) size 50x150
          RenderBlock {DIV} at (0,0) size 50x50 [bgcolor=#008000]
          RenderBlock {DIV} at (0,100) size 50x50 [bgcolor=#FFA500]
    layer at (0,500) size 50x50
      RenderBlock (relative positioned) {DIV} at (0,50) size 50x50 [bgcolor=#0000FF]
  Regions for flow 'article'
    RenderRegion {DIV}
    RenderRegion {DIV} #should-have-layer hasLayer
    RenderRegion {DIV}
