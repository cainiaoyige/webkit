Test that setting and getting grid-column-end and grid-row-end works as expected

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


Test getting -webkit-grid-column-end and -webkit-grid-row-end set through CSS
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-column') is "auto / auto"
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-column-end') is "auto"
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-row') is "auto / auto"
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridElement, '').getPropertyValue('-webkit-grid-row-end') is "auto"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-column') is "auto / 10"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-column-end') is "10"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-row') is "auto / 15"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithPositiveInteger, '').getPropertyValue('-webkit-grid-row-end') is "15"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-column') is "auto / -10"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-column-end') is "-10"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-row') is "auto / -15"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithNegativeInteger, '').getPropertyValue('-webkit-grid-row-end') is "-15"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-column') is "auto / span 2"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-column-end') is "span 2"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-row') is "auto / span 9"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithBeforeSpan, '').getPropertyValue('-webkit-grid-row-end') is "span 9"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-column') is "auto / span 2"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-column-end') is "span 2"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-row') is "auto / span 9"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithAfterSpan, '').getPropertyValue('-webkit-grid-row-end') is "span 9"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-column') is "auto / span 1"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-column-end') is "span 1"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-row') is "auto / span 1"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithOnlySpan, '').getPropertyValue('-webkit-grid-row-end') is "span 1"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-column') is "auto / auto"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-column-end') is "auto"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-row') is "auto / auto"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItemWithAutoElement, '').getPropertyValue('-webkit-grid-row-end') is "auto"

Test the initial value
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column-end') is 'auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column') is 'auto / auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row-end') is 'auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row') is 'auto / auto'

Test getting and setting grid-column-end and grid-row-end through JS
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column') is "auto / 18"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-end') is "18"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row') is "auto / 66"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-end') is "66"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column') is "auto / -55"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-end') is "-55"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row') is "auto / -40"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-end') is "-40"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column') is "auto / span 7"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-end') is "span 7"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row') is "auto / span 2"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-end') is "span 2"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column') is "auto / auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-column-end') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row') is "auto / auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-start') is "auto"
PASS getComputedStyle(gridItem, '').getPropertyValue('-webkit-grid-row-end') is "auto"

Test setting grid-column-end and grid-row-end back to 'auto' through JS
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column-end') is '18'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column') is 'auto / 18'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row-end') is '66'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row') is 'auto / 66'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column-end') is 'auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-column') is 'auto / auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row-end') is 'auto'
PASS getComputedStyle(element, '').getPropertyValue('-webkit-grid-row') is 'auto / auto'
PASS successfullyParsed is true

TEST COMPLETE

